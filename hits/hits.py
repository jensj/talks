"""
[pid: 695209|app: 0|req: 53/53] 130.225.86.27 () {62 vars in 1491 bytes} [Wed May 15 16:31:58 2024] GET /table?sid=11&filter=&stoichiometry=&nspecies=&number_of_layers=&from_binding_energy_gs=&to_binding_energy_gs=&slide_stability=Stable&from_gap_pbe=&to_gap_pbe=&magnetic= => generated 57091 bytes in 2 msecs (HTTP/1.1 200) 2 headers in 82 bytes (1 switches on core 1)
"""
from pathlib import Path
import numpy as np
from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt
import sys
from datetime import datetime

MONTHS = {
    'jan': 1,
    'feb': 2,
    'mar': 3,
    'apr': 4,
    'may': 5,
    'jun': 6,
    'jul': 7,
    'aug': 8,
    'sep': 9,
    'oct': 10,
    'nov': 11,
    'dec': 12}


def T(x):
    # print(x)
    x = x.split('] GET')[0].rsplit('[', 1)[1]
    _, M, d, t, y = x.split()
    M = MONTHS[M.lower()]
    h, m, s = (int(x) for x in t.split(':'))
    return datetime(int(y), M, int(d), h, m, s)


def run(log):
    a = []
    b = []
    for line in log.read_text().splitlines():
        if '] GET /table' in line:
            t = T(line)
            a.append(t)
        elif '] GET /material' in line:
            t = T(line)
            b.append(t)
    return a, b


def plot(a, b):
    label = 'clicks on front page'
    for x in [a, b]:
        print(len(x))
        # x = x[-10000:]
        if 0:
            density = gaussian_kde(x)
            # xs = np.linspace(6, 9, 500)
            xs = np.linspace(2022, 2025, 500)
            density.covariance_factor = lambda: 0.02
            density._compute_covariance()
            plt.plot(xs, density(xs) * len(x) / 31, label=label)
        else:
            hpm = len(x) / (x[-1] - x[0]).days / 24 / 60
            plt.hist(x, bins=100, density=False,
                     label=f'{label} ({hpm:.2f}/min.)', alpha=0.6)
        label = 'clicks on rows'
    plt.legend()
    plt.title('C2DB')
    plt.ylabel('Hits')
    plt.xlabel('Date')
    # plt.axis(ymin=0, ymax=400, xmin=6)
    plt.show()
    # plt.hist([a, b], bins=100, density=False, label=['table', 'summary'])
    # plt.show()


if __name__ == '__main__':
    a, b = run(Path(sys.argv[1]))
    plot(a, b)
