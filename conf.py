import sys
sys.path.append('.')
project = 'talks'
copyright = '2024, Jens Jørgen Mortensen'
author = 'Jens Jørgen Mortensen'
extensions = ['sphinx.ext.extlinks',
              'sphinx.ext.intersphinx',
              'make_slides']
extlinks = {'doi': ('https://doi.org/%s', 'doi: %s')}
templates_path = ['_templates']
exclude_patterns = ['_build', 'README.rst', 'venv']
html_theme = 'bizstyle'
html_theme_options = {'nosidebar': True,
                      'navigation_with_keys': True}
# default_role = 'git'
intersphinx_mapping = {
    'python': ('https://docs.python.org/3.12', None),
    'ase': ('https://wiki.fysik.dtu.dk/ase', None),
    'gpaw': ('https://wiki.fysik.dtu.dk/gpaw', None),
    'numpy': ('https://numpy.org/doc/stable', None),
    'cupy': ('https://docs.cupy.dev/en/stable', None),
    'scipy': ('https://docs.scipy.org/doc/scipy', None)}
