=============================
Online GPAW-developer meeting
=============================

*October 8-9, 2024*

:Program:

    https://gpaw.readthedocs.io/#news

:Tuesday:

    14:00 - 17:05

    7 talks + small break

:Wednesday:

    14:00 - 16:40

    6 talks + small break

Goal of this two-day event:

* raise awareness of who is working on what
* increase collaboration and communication
