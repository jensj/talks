.. role:: strikethrough
.. role:: red
.. role:: green
.. highlight:: bash
.. default-role:: math


==============
PAW potentials
==============

Approximations
--------------

* Frozen core states

* Finite number of projectors, partial waves and pseudo partial waves:

  * Hydrogen: 2 `s`-type, 1 `p`-type
  * Oxygen: 2 `s`-type, 2 `p`-type, 1 `d`-type
  * Copper: 2 `s`-type, 2 `p`-type, 2 `d`-type

* Overlapping augmentation spheres

* Truncated angular momentum expansion of compensation charges


What defines a PAW-potential?
-----------------------------

  ============================  ======================================
  Quantity                      Description
  ============================  ======================================
  `n_c(r)`                      all-electron core density
  `\tilde{n}_c(r)`              pseudo electron core density
  `\bar{v}(r)`                  zero potential
  `\phi_i(\mathbf{r})`          all-electron partial waves
  `\tilde{\phi}_i(\mathbf{r})`  pseudo partial waves
  `\tilde{p}_i(\mathbf{r})`     projector functions
  `g_{\ell m}(\mathbf{r})`      shape function for compensation charge
  `E^\text{kin}_c`              kinetic energy of the core electrons
  `\Delta E^\text{kin}_{ij}`    kinetic energy differences
  ============================  ======================================


Algorithm
---------

* `(\hat T + v - \epsilon)\phi = 0`

* `(\hat T + \tilde v - \epsilon
  + |\tilde p\rangle\Delta H\langle\tilde p|
  - |\tilde p\rangle\epsilon\Delta S\langle\tilde p|)\tilde\phi = 0`

* Construct smooth `\tilde\phi = \phi` for `r>r_c`

* Construct smooth `\tilde n_c = n_c` for `r>r_c`

* `\tilde n = 2f\tilde\phi^2 + \tilde n_c`

* Solve for `\bar v` using:

  * `\tilde v = v_{xc}[\tilde n] + v_H[\tilde n] + \bar v`

  * `\tilde v = (\epsilon - \hat T)\tilde\phi / \tilde\phi`

* `\tilde p \propto (\hat T + \tilde v - \epsilon)\tilde\phi`,
  `\langle\tilde p|\tilde\phi\rangle=1`

* Apply Fourier-filter to `\bar v` and `\tilde p`

* Done!


The ``generator2.py`` + ``aeatom.py`` code
------------------------------------------

Our current PAW-potentials were generated using the ``gpaw.atom.generator``
and ``gpaw.atom.all_electron`` modules.
The ``gpaw.atom.generator2`` and ``gpaw.atom.aeatom`` modules are
for experimenting with generation of new and better PAW-potentials.

Solving the radial equations (non-relativistic, scalar-relativistic and Dirac):

* Basis set of gaussians: diagonalize `\langle\alpha|\hat H|\beta\rangle`, ...
* Shooting method: Integrate radial equation inwards and outwards and
  match, ...


(Automatic) generation of PAW-potentials
----------------------------------------

Parameters:

* reference energies `\epsilon_{n\ell}`
* cutoff radii for `\tilde n_c`, `\bar v` and `\tilde p_{n\ell}`

Optimize for:

* Nice logarithmic derivatives `(d\phi/dr)/\phi`
* Fast convergence with respect to PW cutoff
* Low egg-box errors
* Nice SCF convergence
* Good lattice constants
* ...
