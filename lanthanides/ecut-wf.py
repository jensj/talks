from myqueue.workflow import run
from pathlib import Path


def workflow():
    # for symbol in ['La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd',
    #                'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu']:
    p = Path.cwd()
    # r = p.name
    symbol = p.parent.name
    for name in 'Diamond,BCC'.split(','):
        for ecut in range(400, 1300, 100):
            run(shell='cg',
                args=['one-shot',
                      f'{symbol}-{name}.txt@3',
                      '--pot-path=.:',
                      f'--ecut={ecut}',
                      f'--name={name}-{ecut}'],
                name=f'{name}-{ecut}',
                cores=24,
                tmax='4h')


def plot():
    import json
    import numpy as np
    import matplotlib.pyplot as plt

    EC = list(range(400, 1300, 100))
    for r in [2.2, 2.3, 2.4, 2.5]:
        E = []
        for name in 'Diamond,BCC'.split(','):
            E.append([])
            for ecut in EC:
                d = json.loads(
                    Path(f'Tb/{r:.1f}/{name}-{ecut}.json').read_text())
                e = d['energy']
                E[-1].append(e)
        dia, bcc = np.array(E) * 1000
        dia *= 0.5
        dia -= bcc
        plt.plot(EC, bcc - bcc[-1], label=f'BCC {r}')
        plt.plot(EC, dia - dia[-1], label=f'Diamond-BCC {r}')
    plt.legend()
    plt.xlabel('ecut [eV]')
    plt.ylabel('error [meV/atom]')
    plt.ylim(-5, 15)
    plt.xlim(400, 1200)
    plt.hlines(0.0, xmin=400, xmax=1200)
    plt.title(f'Tb: Diamond - BCC = {dia[-1]:.0f} meV/atom')
    plt.show()


if __name__ == '__main__':
    plot()
