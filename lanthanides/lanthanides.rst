.. role:: strikethrough
.. role:: red
.. role:: green
.. highlight:: bash


==============================
PAW-potentials for Lanthanides
==============================


Terbium
=======

.. code-block:: bash

   $ gpaw dataset Tb -s -fPBE -r2.2 -P 5s,6s,5p,6p,5d,d,4f,f,G -lspdfg -p
   ...
    state  occupation         eigenvalue          <r>
    nl                  [Hartree]        [eV]    [Bohr]
   -----------------------------------------------------
    1s        2.000  -1905.880174   -51861.64114  0.022
    2s        2.000   -315.934090    -8597.00448  0.091
    2p        6.000   -280.713580    -7638.60559  0.081
    3s        2.000    -70.072418    -1906.76761  0.239
    3p        6.000    -59.212649    -1611.25825  0.238
    3d       10.000    -45.071652    -1226.46212  0.217
    4s        2.000    -13.877860     -377.63580  0.541
    4p        6.000    -10.303339     -280.36813  0.574
    4d       10.000     -5.399147     -146.91826  0.624
    5s        2.000     -1.745045      -47.48510  1.327
    5p        6.000     -0.912523      -24.83103  1.573
    6s        2.000     -0.136165       -3.70523  4.163
    4f        9.000     -0.111143       -3.02436  0.910
    5d        0.000     -0.047799       -1.30069  3.461
    6p        0.000     -0.043084       -1.17237  5.973
   ...
   Projectors:
    state  occ         energy             norm        rcut
    nl            [Hartree]  [eV]      [electrons]   [Bohr]
   ----------------------------------------------------------
    5s    2.00  -1.745045  -47.48510      1.096       2.20
    6s    2.00  -0.136165   -3.70523      1.003       2.20
    5p    6.00  -0.912523  -24.83103      0.855       2.20
    6p    0.00  -0.043084   -1.17237      0.992       2.20
    5d    0.00  -0.047799   -1.30069      0.972       2.20
     d           0.952201   25.91070                  2.20
    4f    9.00  -0.111143   -3.02436      0.120       2.20
     f           0.888857   24.18702                  2.20

Logarithmic derivatives:

  .. math::

    \frac{d\phi_{\ell\epsilon}(r)/dr|_{r=r_c}}{\phi_{\ell\epsilon}(r_c)} =
    d\log\phi_{\ell\epsilon}(r)/dr|_{r=r_c}

.. figure:: logderiv.png


All lanthanides?
================

.. code-block:: python

    from subprocess import run
    for symbol in ['La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd',
                   'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu']:
        for r in [2.2, 2.3, 2.4, 2.5]:
            p = Path(symbol) / f'{r:.1f}'
            p.mkdir(parents=True, exist_ok=True)
            run(f'gpaw dataset {symbol} -s -fPBE -w -r{r} '
                '-P 5s,6s,5p,6p,5d,d,4f,f,G',
                shell=True, cwd=p)


ACWF bechmark
=============

https://acwf-verification.materialscloud.org/

10 bulk crystasls:
XO3, XO, X4O6, XO2, X4O10, X2O, Diamond, FCC, SC, BCC

.. figure:: acwf.png


Plane-wave convergence
======================

.. figure:: ecut.png
