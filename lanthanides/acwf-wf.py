from pathlib import Path

import numpy as np
from myqueue.workflow import run


def workflow():
    # for symbol in ['La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd',
    #                'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu']:
    p = Path.cwd()
    r = p.name
    symbol = p.parent.name
    for name in 'XO3,XO,X4O6,XO2,X4O10,X2O,Diamond,FCC,SC,BCC'.split(','):
        # if Path(f'{symbol}-{name}.json').is_file():
        #     continue
        run(shell='cg',
            args=['acwf', symbol, name, '--pot-path=.:'],
            # '--use-old-gpaw'],
            name=f'{symbol}-{name}-{r}',
            cores=24,
            tmax='4h')


def pot():
    from subprocess import run
    for symbol in ['La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd',
                   'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu']:
        for r in [2.2, 2.3, 2.4, 2.5]:
            p = Path(symbol) / f'{r:.1f}'
            p.mkdir(parents=True, exist_ok=True)
            run(f'gpaw dataset {symbol} -s -fPBE -w -r{r}',
                shell=True, cwd=p)


def plot1():
    import json
    from collections import defaultdict
    from pathlib import Path
    S = defaultdict(lambda: defaultdict(dict))
    for p in Path().glob('??/?.?/??-*.json'):
        print(p)
        symbol = p.parent.parent.name
        if not p.name.startswith(symbol):
            continue
        r = p.parent.name
        xtal = p.stem.split('-')[1]
        results = json.loads(p.read_text())
        S[r][symbol][xtal] = results['strain']
    import pickle
    S = dict(S)
    Path('strains.pckl').write_bytes(pickle.dumps(S))


def plot2():
    import pickle
    from pathlib import Path

    import matplotlib.pyplot as plt
    from ase.data import chemical_symbols

    S = pickle.loads(Path('strains.pckl').read_bytes())
    Z = chemical_symbols.index('La')
    c = 0
    for r, A in S.items():
        E1 = []
        E2 = []
        N = 0
        for s in chemical_symbols[Z:Z + 15]:
            strains = A[s]
            n = len(strains)
            E1.append(sum(abs(s) for s in strains) / n * 100)
            E2.append(max(abs(s) for s in strains) * 100)
            N += n
        plt.plot(E1, color=f'C{c}', label=f'{r}, mean-abs ({N})')
        plt.plot(E2, '--', color=f'C{c}', label=f'{r}, max-abs')
        c += 1
    plt.legend()
    plt.xlabel('4f electrons')
    plt.ylabel('lattice constant error [%]')
    plt.savefig('acwf.png')
    plt.show()


def plot3():
    import pickle
    from pathlib import Path

    import matplotlib.pyplot as plt
    from ase.data import chemical_symbols

    xtals = 'XO3,XO,X2O3,XO2,X2O5,X2O,Diamond,FCC,SC,BCC'.split(',')
    S = pickle.loads(Path('strains.pckl').read_bytes())
    Z = chemical_symbols.index('La')
    markers = 'ov^<>s*+xD'
    for xtal in xtals:
        for r, A in S.items():
            print(r)
            E = []
            for s in chemical_symbols[Z:Z + 15]:
                strain = A[s].get(xtal, np.nan)
                E.append(strain * 100)
            break
        xtal = ''.join(f'_{x}' if x.isdigit() else x for x in xtal)
        print(xtal)
        plt.plot(E, marker=markers[0], label=f'${xtal}$')
        markers = markers[1:]
    plt.legend()
    plt.xlabel('4f electrons')
    plt.xticks(range(15), chemical_symbols[Z:Z + 15])
    plt.ylabel('lattice constant error [%]')
    # plt.savefig('acwf.png')
    plt.show()


if __name__ == '__main__':
    plot3()
