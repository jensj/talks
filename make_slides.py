from pathlib import Path


def make_slides():
    lines = Path('dev24/talk.rst').read_text().splitlines()
    linenumbers = [n - 1
                   for n, line in enumerate(lines)
                   if line.startswith('===')]
    linenumbers.append(len(lines))

    header = '\n'.join(lines[:linenumbers[0] - 2]) + '\n'

    slidenumber = 1
    Path('dev24').mkdir(exist_ok=True)
    for n1, n2 in zip(linenumbers[:-1], linenumbers[1:]):
        Path(f'dev24/slide-{slidenumber:02}.rst').write_text(
            header + '\n'.join(lines[n1:n2]))
        slidenumber += 1


def setup(app):
    # make_slides()
    if 0:
        if not Path('lumi/pie.svg').is_file():
            pass
            # from pies import make_pies
            # make_pies()
